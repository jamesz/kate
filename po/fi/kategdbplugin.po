# Finnish messages for kategdbplugin.
# Copyright © 2011, 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdebase package.
# Jorma Karvonen <karvonen.jorma@gmail.com>, 2011-2012.
# Lasse Liehu <lasse.liehu@gmail.com>, 2012, 2013, 2014, 2015, 2016.
# Tommi Nieminen <translator@legisign.org>, 2020, 2022, 2023.
#
# KDE Finnish translation sprint participants:
# Author: Lliehu
# Author: Niklas Laxström
msgid ""
msgstr ""
"Project-Id-Version: kategdbplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-22 00:39+0000\n"
"PO-Revision-Date: 2023-09-29 10:11+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:21:57+0000\n"
"X-Generator: Lokalize 22.12.3\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Komento"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Lähdetiedostojen hakupolut"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Paikallinen sovellus"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Etä-TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Etäsarjaportti"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Verkkonimi"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Portti"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baudi"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absoluuttinen-etuliite"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-hakupolku"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Mukautetut alustuskomennot"

#: backend.cpp:24 backend.cpp:49 dapbackend.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Virheenpaikannusistunto on käynnissä. Suorita uudelleen tai pysäytä nykyinen "
"istunto."

#: configview.cpp:93
#, kde-format
msgid "Add new target"
msgstr "Lisää uusi kohde"

#: configview.cpp:97
#, kde-format
msgid "Copy target"
msgstr "Kopioi kohde"

#: configview.cpp:101
#, kde-format
msgid "Delete target"
msgstr "Poista kohde"

#: configview.cpp:106
#, kde-format
msgid "Executable:"
msgstr "Ohjelmatiedosto:"

#: configview.cpp:126
#, kde-format
msgid "Working Directory:"
msgstr "Työhakemisto:"

#: configview.cpp:134
#, kde-format
msgid "Process Id:"
msgstr "Prosessitunniste:"

#: configview.cpp:139
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumentit:"

#: configview.cpp:142
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Pidä kohdistus"

#: configview.cpp:143
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Pidä kohdistus komentorivillä"

#: configview.cpp:145
#, kde-format
msgid "Redirect IO"
msgstr "Uudelleen ohjattu siirräntä"

#: configview.cpp:146
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr ""
"Ohjaa virheenpaikannuksessa olevien ohjelmien siirräntä uudelleen erilliseen "
"välilehteen"

#: configview.cpp:148
#, kde-format
msgid "Advanced Settings"
msgstr "Lisäasetukset"

#: configview.cpp:232
#, kde-format
msgid "Targets"
msgstr "Kohteet"

#: configview.cpp:525 configview.cpp:538
#, kde-format
msgid "Target %1"
msgstr "Kohde %1"

#: dapbackend.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "DAP-taustajärjestelmä epäonnistui"

#: dapbackend.cpp:211
#, kde-format
msgid "program terminated"
msgstr "ohjelma päätettiin"

#: dapbackend.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "pyydetään yhteyden katkaisua"

#: dapbackend.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "pyydetään sammutusta"

#: dapbackend.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP-taustajärjestelmä: %1"

#: dapbackend.cpp:270 gdbbackend.cpp:653
#, kde-format
msgid "stopped (%1)."
msgstr "pysäytetty (%1)."

#: dapbackend.cpp:278 gdbbackend.cpp:657
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Aktiivinen säie: %1 (kaikki säikeet pysäytetty)."

#: dapbackend.cpp:280 gdbbackend.cpp:659
#, kde-format
msgid "Active thread: %1."
msgstr "Aktiivinen säie: %1."

#: dapbackend.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Keskeytyskohdat saavutettu:"

#: dapbackend.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(jatkettiin) säie %1"

#: dapbackend.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "jatkettiin kaikkia säikeitä"

#: dapbackend.cpp:316
#, kde-format
msgid "(running)"
msgstr "(käynnissä)"

#: dapbackend.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** palvelinyhteys sulkeutui ***"

#: dapbackend.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "ohjelma päättyi koodilla %1"

#: dapbackend.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** odotetaan käyttäjän toimia ***"

#: dapbackend.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "virhe vastauksessa: %1"

#: dapbackend.cpp:445
#, kde-format
msgid "important"
msgstr "tärkeä"

#: dapbackend.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetria"

#: dapbackend.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "virheenpaikannusprosessi [%1] %2"

#: dapbackend.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "virheenpaikannusprosessi %1"

#: dapbackend.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Aloitustapa: %1"

#: dapbackend.cpp:479
#, kde-format
msgid "thread %1"
msgstr "säie %1"

#: dapbackend.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "keskeytyskohta asetettu"

#: dapbackend.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "keskeytyskohta poistettu"

#: dapbackend.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) keskeytyskohta"

#: dapbackend.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<ei suoritettu>"

#: dapbackend.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "palvelimen ominaisuudet"

#: dapbackend.cpp:742
#, kde-format
msgid "supported"
msgstr "tuettu"

#: dapbackend.cpp:742
#, kde-format
msgid "unsupported"
msgstr "ei tuettu"

#: dapbackend.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "ehdolliset keskeytyskohdat"

#: dapbackend.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "funktion keskeytyskohdat"

# *** TARKISTA: Ei aavistustakaan, mitä ”hit” tässä on, mutta conditional ≠ hit conditional, joten jotakin pitää keksiä.
#: dapbackend.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "osumaehtoiset keskeytyskohdat"

#: dapbackend.cpp:748
#, kde-format
msgid "log points"
msgstr "lokipisteet"

#: dapbackend.cpp:748
#, kde-format
msgid "modules request"
msgstr "moduulipyyntö"

#: dapbackend.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "goto-määränpääpyyntö"

#: dapbackend.cpp:750
#, kde-format
msgid "terminate request"
msgstr "lopeta pyyntö"

#: dapbackend.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "lopeta jäljitettävä"

#: dapbackend.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "syntaksivirhe: lauseketta ei löytynyt"

#: dapbackend.cpp:976 dapbackend.cpp:1011 dapbackend.cpp:1049
#: dapbackend.cpp:1083 dapbackend.cpp:1119 dapbackend.cpp:1155
#: dapbackend.cpp:1191 dapbackend.cpp:1291 dapbackend.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "syntaksivirhe: %1"

#: dapbackend.cpp:984 dapbackend.cpp:1019 dapbackend.cpp:1298
#: dapbackend.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "virheellinen rivi: %1"

#: dapbackend.cpp:991 dapbackend.cpp:996 dapbackend.cpp:1026
#: dapbackend.cpp:1031 dapbackend.cpp:1322 dapbackend.cpp:1327
#: dapbackend.cpp:1368 dapbackend.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "tiedostoa ei määritetty: %1"

#: dapbackend.cpp:1061 dapbackend.cpp:1095 dapbackend.cpp:1131
#: dapbackend.cpp:1167 dapbackend.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "virheellinen säikeen tunniste: %1"

#: dapbackend.cpp:1067 dapbackend.cpp:1101 dapbackend.cpp:1137
#: dapbackend.cpp:1173 dapbackend.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "säikeen tunnistetta ei annettu: %1"

#: dapbackend.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Käytettävissä olevat komennot:"

#: dapbackend.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "palvelin ei tue ehdollisia keskeytyskohtia"

#: dapbackend.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr "palvelin ei tue osumaehtoisia keskeytyskohtia"

#: dapbackend.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "rivillä %1 on jo keskeytyskohta"

#: dapbackend.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "keskeytyskohtaa ei löydy (%1:%2)"

#: dapbackend.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Nykyinen säie:"

#: dapbackend.cpp:1392 dapbackend.cpp:1399 dapbackend.cpp:1423
#, kde-format
msgid "none"
msgstr "ei mitään"

#: dapbackend.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Nykyinen kehys:"

#: dapbackend.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Istunnon tila:"

#: dapbackend.cpp:1405
#, kde-format
msgid "initializing"
msgstr "Alustetaan"

#: dapbackend.cpp:1408
#, kde-format
msgid "running"
msgstr "käynnissä"

#: dapbackend.cpp:1411
#, kde-format
msgid "stopped"
msgstr "pysäytetty"

#: dapbackend.cpp:1414
#, kde-format
msgid "terminated"
msgstr "päätetty"

#: dapbackend.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "yhteys katkaistu"

#: dapbackend.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "ruumiinavaus"

#: dapbackend.cpp:1476
#, kde-format
msgid "command not found"
msgstr "komentoa ei löydy"

#: dapbackend.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "puuttuva säietunniste"

#: dapbackend.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "tapetaan taustajärjestelmää"

#: dapbackend.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Nykyinen kehys [%3]: %1:%2 (%4)"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Virheenpaikannussovittimen käyttäjäasetukset"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Asetustiedosto:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Virheenpaikannussovittimen oletusasetukset"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Virheenpaikannin"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "Ei validoitavaa JSON-dataa."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "JSON-data on kelvollista."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "JSON-data on kelvotonta: ei JSON-oliota"

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "JSON-data on kelvotonta: %1"

#: gdbbackend.cpp:35
#, kde-format
msgid "Locals"
msgstr "Paikalliset"

#: gdbbackend.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Suoritinrekisterit"

#: gdbbackend.cpp:158
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Aseta ohjelmatiedosto Virheenpaikannus-paneelin Asetukset-välilehdellä."

#: gdbbackend.cpp:167
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Virheenjäljitintä ei ole valittu. Valitse se Virheenpaikannus-paneelin "
"Asetukset-välilehdeltä."

#: gdbbackend.cpp:176
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Virheenjäljitintä ei löydy. Varmista, että se on järjestelmään asennettu. "
"Valittu vianjäljitin on ”%1”"

#: gdbbackend.cpp:382
#, kde-format
msgid "Could not start debugger process"
msgstr "Ei voitu käynnistää virheenpaikanninprosessia"

#: gdbbackend.cpp:440
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb päättyi normaalisti ***"

#: gdbbackend.cpp:646
#, kde-format
msgid "all threads running"
msgstr "kaikki säikeet käynnissä"

#: gdbbackend.cpp:648
#, kde-format
msgid "thread(s) running: %1"
msgstr "säikeitä käynnissä: %1"

#: gdbbackend.cpp:678
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Nykyinen kehys: %1:%2"

#: gdbbackend.cpp:705
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Kone: %1. Kohde: %1"

#: gdbbackend.cpp:1375
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Ei voitu jäsentää viimeisintä vastausta: %1. Liian monta peräkkäistä "
"virhettä. Lopetetaan."

#: gdbbackend.cpp:1377
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Ei voitu jäsentää viimeisintä vastausta: %1"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symboli"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Arvo"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tyyppi"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "indeksoidut kohteet"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "nimetyt kohteet"

#: plugin_kategdb.cpp:103
#, kde-format
msgid "Kate Debug"
msgstr "Kate-virheenpaikannin"

#: plugin_kategdb.cpp:107
#, kde-format
msgid "Debug View"
msgstr "Virheenpaikannusnäkymä"

#: plugin_kategdb.cpp:107 plugin_kategdb.cpp:340
#, kde-format
msgid "Debug"
msgstr "Virheenpaikannus"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:113
#, kde-format
msgid "Locals and Stack"
msgstr "Paikalliset muuttujat ja pino"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nro"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Kehys"

#: plugin_kategdb.cpp:197
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Virheenpaikannustuloste"

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Asetukset"

#: plugin_kategdb.cpp:240
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Tiedostoa ei voitu avata:</title><nl/>%1<br/>Kokeile lisätä hakupolku "
"kohdassa Lisäasetukset → Lähdetiedostojen hakupolut"

#: plugin_kategdb.cpp:265
#, kde-format
msgid "Start Debugging"
msgstr "Aloita virheenpaikannus"

#: plugin_kategdb.cpp:275
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Tapa / pysäytä virheenpaikannus"

#: plugin_kategdb.cpp:282
#, kde-format
msgid "Continue"
msgstr "Jatka"

#: plugin_kategdb.cpp:288
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Vaihda keskeytyskohtaa / keskeytystä"

#: plugin_kategdb.cpp:294
#, kde-format
msgid "Step In"
msgstr "Astu sisään"

#: plugin_kategdb.cpp:301
#, kde-format
msgid "Step Over"
msgstr "Astu yli"

#: plugin_kategdb.cpp:308
#, kde-format
msgid "Step Out"
msgstr "Astu ulos"

#: plugin_kategdb.cpp:315 plugin_kategdb.cpp:347
#, kde-format
msgid "Run To Cursor"
msgstr "Suorita kohdistimeen"

#: plugin_kategdb.cpp:322
#, kde-format
msgid "Restart Debugging"
msgstr "Käynnistä virheenpaikannus uudelleen"

#: plugin_kategdb.cpp:330 plugin_kategdb.cpp:349
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Siirrä ohjelmalaskuri"

#: plugin_kategdb.cpp:335
#, kde-format
msgid "Print Value"
msgstr "Tulosta arvo"

#: plugin_kategdb.cpp:344
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:346
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:428 plugin_kategdb.cpp:444
#, kde-format
msgid "Insert breakpoint"
msgstr "Lisää keskeytyskohta"

#: plugin_kategdb.cpp:442
#, kde-format
msgid "Remove breakpoint"
msgstr "Poista keskeytyskohta"

#: plugin_kategdb.cpp:571
#, kde-format
msgid "Execution point"
msgstr "Suorituspiste"

#: plugin_kategdb.cpp:710
#, kde-format
msgid "Thread %1"
msgstr "Säie %1"

#: plugin_kategdb.cpp:810
#, kde-format
msgid "IO"
msgstr "Siirräntä"

#: plugin_kategdb.cpp:894
#, kde-format
msgid "Breakpoint"
msgstr "Keskeytyskohta"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Virheenpaikannus"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Virheenpaikannusliitännäinen"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Tommi Nieminen"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "translator@legisign.org"

#~ msgid "GDB Integration"
#~ msgstr "GDB-integraatio"

#~ msgid "Kate GDB Integration"
#~ msgstr "Kate GDB -integraatio"

#~ msgid "&Target:"
#~ msgstr "&Kohde:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "&Argumenttiluettelo:"

#~ msgid "Remove Argument List"
#~ msgstr "Poista argumenttiluettelo"

#~ msgid "Arg Lists"
#~ msgstr "Argumenttiluettelot"

# Translation of katekonsoleplugin.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2007, 2009, 2013, 2014, 2017.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-17 00:39+0000\n"
"PO-Revision-Date: 2017-12-31 22:42+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: kateconsole.cpp:57
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "Nemate dovoljno karme za pristup školjci ili emulaciji terminala"

#: kateconsole.cpp:105 kateconsole.cpp:135 kateconsole.cpp:703
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:144
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Prosledi u terminal"

#: kateconsole.cpp:148
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "&Sinhronizuj terminal sa dokumentom"

#: kateconsole.cpp:152
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Izvrši dokument"

#: kateconsole.cpp:157 kateconsole.cpp:521
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "&Fokusiraj terminal"

#: kateconsole.cpp:163
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Fokusiraj terminal"

#: kateconsole.cpp:169
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Pipe to Terminal"
msgctxt "@action"
msgid "&Split Terminal Vertically"
msgstr "&Prosledi u terminal"

#: kateconsole.cpp:174
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Horizontally"
msgstr ""

#: kateconsole.cpp:179
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "&New Terminal Tab"
msgstr "&Fokusiraj terminal"

#: kateconsole.cpp:320
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""

#: kateconsole.cpp:396
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Želite li zaista da proslijedite tekst u konzolu? Time će se izvršiti svaka "
"sadržana naredba, uz vaše korisničke dozvole."

#: kateconsole.cpp:397
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Proslijediti u terminal?"

#: kateconsole.cpp:398
#, kde-format
msgid "Pipe to Terminal"
msgstr "Proslijedi u terminal"

#: kateconsole.cpp:426
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Izvinite, ne mogu da uđem u ‘%1’"

#: kateconsole.cpp:462
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Nije lokalni fajl: %1"

#: kateconsole.cpp:495
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Želite li zaista da izvršite ovaj dokument? Izvršila bi se sledeća naredba, "
"pod vašim korisničkim dozvolama, u terminalu:\n"
"%1"

#: kateconsole.cpp:502
#, kde-format
msgid "Run in Terminal?"
msgstr "Izvršiti u terminalu?"

#: kateconsole.cpp:503
#, kde-format
msgid "Run"
msgstr "Izvrši"

#: kateconsole.cpp:518
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Pipe to Terminal"
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Prosledi u terminal"

#: kateconsole.cpp:529
#, fuzzy, kde-format
#| msgid "Defocus Terminal"
msgid "Defocus Terminal Panel"
msgstr "&Defokusiraj terminal"

#: kateconsole.cpp:530 kateconsole.cpp:531
#, fuzzy, kde-format
#| msgid "Focus Terminal"
msgid "Focus Terminal Panel"
msgstr "Fokusiraj terminal"

#: kateconsole.cpp:636
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr "&Automatski sinhronizuj terminal sa tekućim dokumentom kada je moguće"

# >> @title:window
#: kateconsole.cpp:640 kateconsole.cpp:661
#, kde-format
msgid "Run in terminal"
msgstr "Izvršavanje u terminalu"

#: kateconsole.cpp:642
#, kde-format
msgid "&Remove extension"
msgstr "&Ukloni nastavak"

#: kateconsole.cpp:647
#, kde-format
msgid "Prefix:"
msgstr "Prefiks:"

#: kateconsole.cpp:655
#, kde-format
msgid "&Show warning next time"
msgstr "&Upozori sledeći put"

#: kateconsole.cpp:657
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"Sledeći put kada „%1“ treba da se izvrši, pojaviće se prozor sa upozorenjem, "
"gde će biti dat pregled naredbe koja se šalje u terminal."

# literal-segment: EDITOR
#: kateconsole.cpp:668
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Postavi promjenljivu &okruženja $EDITOR na „kate -b“"

#: kateconsole.cpp:671
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr "Važno: dokument mora biti zatvoren da bi se konzolni program nastavio."

#: kateconsole.cpp:674
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr ""

#: kateconsole.cpp:677
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:708
#, kde-format
msgid "Terminal Settings"
msgstr "Postavke terminala"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Alatke"

#~ msgid "Kate Terminal"
#~ msgstr "Katein terminal"
